object Dependencies {

    // ANDROID CORE
    const val CORE_KTX = "androidx.core:core-ktx:1.8.0"
    const val ANDROID_APP_COMPAT = "androidx.appcompat:appcompat:1.5.1"
    const val ANDROID_MATERIAL = "com.google.android.material:material:1.6.1"
    const val ANDROID_CONSTRAINTLAYOUT = "androidx.constraintlayout:constraintlayout:2.1.4"
    const val LIVEDATA_KTX = "androidx.lifecycle:lifecycle-livedata-ktx:${BuildDependenciesVersions.LIFECYCLE}"
    const val VIEWMODEL_KTX = "androidx.lifecycle:lifecycle-viewmodel-ktx:${BuildDependenciesVersions.LIFECYCLE}"
    const val NAV_FRAGMENT_KTX = "androidx.navigation:navigation-fragment-ktx:${BuildDependenciesVersions.NAV}"
    const val NAV_UI_KTX = "androidx.navigation:navigation-ui-ktx:${BuildDependenciesVersions.NAV}"
    const val LEGACY = "androidx.legacy:legacy-support-v4:1.0.0"

    const val GLIDE = "com.github.bumptech.glide:glide:4.13.0"
    const val SWIPE_REFRESH = "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"

    // KOTLIN COROUTINE
    const val COROUTINES =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${BuildDependenciesVersions.COROUTINES}"
    const val COROUTINES_ANDROID =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${BuildDependenciesVersions.COROUTINES}"


    // RETROFIT
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${BuildDependenciesVersions.RETROFIT}"
    const val RETROFIT_GSON =
        "com.squareup.retrofit2:converter-gson:${BuildDependenciesVersions.RETROFIT}"

    const val ROOM_KTX = "androidx.room:room-ktx:${BuildDependenciesVersions.ROOM_VERSION}"
    const val ROOM_COMPILER = "androidx.room:room-compiler:${BuildDependenciesVersions.ROOM_VERSION}"

    // LOGGING
    const val LOGGING =
        "com.squareup.okhttp3:logging-interceptor:${BuildDependenciesVersions.LOGGING}"
    const val OKHTTP = "com.squareup.okhttp3:okhttp:${BuildDependenciesVersions.OKHTTP}"

    // KOIN
    const val KOIN_CORE = "io.insert-koin:koin-core:${BuildDependenciesVersions.KOIN}"
    const val KOIN_ANDROID = "io.insert-koin:koin-android:${BuildDependenciesVersions.KOIN}"

    //TEST
    const val JUNIT = "junit:junit:4.13.2"
    const val MOCK = "org.mockito:mockito-core:4.3.1"
    const val CORE_TEST = "androidx.arch.core:core-testing:2.1.0"
    const val MOCK_KOTLIN = "org.mockito.kotlin:mockito-kotlin:4.0.0"
    const val MOCK_IO = "io.mockk:mockk:1.12.2"
    const val COROUTINE_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.0"
    const val MOCK_SERVER = "com.squareup.okhttp3:mockwebserver:5.0.0-alpha.2"
}

object BuildDependenciesVersions {
    const val COROUTINES = "1.3.9"
    const val KOIN = "3.1.2"
    const val RETROFIT = "2.9.0"
    const val LOGGING = "4.9.0"
    const val OKHTTP = "4.9.1"
    const val LIFECYCLE = "2.5.1"
    const val NAV= "2.5.2"
    const val ROOM_VERSION = "2.4.2"
}