plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdk = 32

    defaultConfig {
        applicationId = "com.joule.nytimes"
        minSdk = 23
        targetSdk = 32
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {

        getByName("debug") {
            buildConfigField("String", "BASE_URL", "\"https://api.nytimes.com\"")
            buildConfigField("String", "API_KEY", "\"2ZcwauGDIzoYKDLttvZHrTKtnG6uF0xo\"")
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )

        }

        getByName("release") {
            buildConfigField("String", "BASE_URL", "\"https://api.nytimes.com\"")
            buildConfigField("String", "API_KEY", "\"2ZcwauGDIzoYKDLttvZHrTKtnG6uF0xo\"")
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )

        }

    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    with(Dependencies) {
        // android core
        implementation(LEGACY)
        implementation(ANDROID_APP_COMPAT)
        implementation(LIVEDATA_KTX)
        implementation(CORE_KTX)
        implementation(ANDROID_MATERIAL)
        implementation(ANDROID_CONSTRAINTLAYOUT)
        implementation(VIEWMODEL_KTX)
        implementation(NAV_FRAGMENT_KTX)
        implementation(NAV_UI_KTX)
        implementation(GLIDE)
        implementation(SWIPE_REFRESH)

        // dependency
        implementation(KOIN_ANDROID)
        implementation(KOIN_CORE)

        // coroutines
        implementation(COROUTINES)
        implementation(COROUTINES_ANDROID)

        // remote
        implementation(RETROFIT)
        implementation(RETROFIT_GSON)
        implementation(LOGGING)
        implementation(OKHTTP)

        //room
        implementation(ROOM_KTX)
        kapt(ROOM_COMPILER)

        testImplementation(JUNIT)
        testImplementation(MOCK)
        testImplementation(MOCK_IO)
        testImplementation(MOCK_KOTLIN)
        testImplementation(MOCK_SERVER)
        testImplementation(CORE_TEST)
        testImplementation(COROUTINE_TEST)
    }

    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
}