package com.joule.nytimes.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.joule.nytimes.domain.NytimesRepository
import com.joule.nytimes.local.NewsDao
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.model.Resources
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@RunWith(MockitoJUnitRunner::class)
internal class HomeViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: NytimesRepository

    @Mock
    lateinit var newsDao: NewsDao

    @Mock
    private lateinit var observerEntity: Observer<Resources<List<NewsEntity>>>

    @OptIn(ExperimentalCoroutinesApi::class)
    private val dispatcher = UnconfinedTestDispatcher()

    private lateinit var homeViewModel: HomeViewModel

    val dummyNews = Resources.Success(listOf(NewsEntity(title = "title", id = 0, section = "section", content = "content", url = "url", isFav = 0, thumbnail = "thumbnail")))

    @Before
    fun setUp() {
        observerEntity = mock()
        homeViewModel = HomeViewModel(repository, newsDao, dispatcher)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `given homeviewmodel getAllNews from newsDao`(){
        runTest {
            val pageOffset = 1
            Mockito.`when`(repository.getAllNews(pageOffset)).thenReturn(dummyNews)
            homeViewModel.news.observeForever(observerEntity)
            homeViewModel.getAllNews(pageOffset)
            verify(observerEntity).onChanged(dummyNews)
        }

    }
}