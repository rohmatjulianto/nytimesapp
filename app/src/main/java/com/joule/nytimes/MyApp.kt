package com.joule.nytimes

import android.app.Application
import com.joule.nytimes.module.dbModule
import com.joule.nytimes.module.networkModule
import com.joule.nytimes.module.repositoryModule
import com.joule.nytimes.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp : Application(){
    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@MyApp)
            modules(networkModule)
            modules(dbModule)
            modules(repositoryModule)
            modules(viewModelModule)
        }
    }
}