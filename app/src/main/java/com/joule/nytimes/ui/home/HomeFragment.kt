package com.joule.nytimes.ui.home

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.joule.nytimes.databinding.FragmentHomeBinding
import com.joule.nytimes.model.Resources
import com.joule.nytimes.ui.adapter.NewsAdapter
import com.joule.nytimes.ui.adapter.NewsClickListener
import com.joule.nytimes.utils.ExtensionsUtils.showToast
import org.koin.android.ext.android.inject
import java.net.UnknownHostException

class HomeFragment : Fragment(), NewsClickListener {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    var state: Boolean = false
    var pageOffset: Int = 1
    var sizeNews : Int = 0
    val viewModel: HomeViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureSearchView()
        configureScroll()
        configureSwipe()

        val newsAdapter = NewsAdapter(this)
        binding.rvList.apply {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
                layoutManager = GridLayoutManager(context, 2)
            }else{

                layoutManager = LinearLayoutManager(context)
            }
            adapter = newsAdapter
        }

        viewModel.news.observe(viewLifecycleOwner) { result ->
            state = true
            binding.swipe.isRefreshing = false
            binding.pbBar.visibility = View.GONE

            when (result) {
                is Resources.Success -> {
                    result.data?.let {
                        newsAdapter.submitList(it)
                        if (it.size != sizeNews){
                            sizeNews = it.size
                            pageOffset++
                        }
                    }

                }
                is Resources.Error -> {
                    when(result.e){
                        is UnknownHostException -> {
                            viewModel.getAllNewsSaved(pageOffset)
                            context.showToast("Koneksi tidak tersedia, hanya menampilkan yang terdapat di local")
                        }
                        else ->{
                            context.showToast(result.e?.message)
                        }

                    }

                }
                is Resources.Fault ->{
                    context.showToast(result.msg)
                }
            }
        }
    }


    override fun onClickNewsTimes(title: String?) {
        val action = HomeFragmentDirections.actionNavigationHomeToDetailActivity(title)
        findNavController().navigate(action)
    }

    fun configureScroll() {
        binding.rvList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == 0) {
                    if (state) {
                        state = false
                        binding.pbBar.visibility = View.VISIBLE
                        viewModel.getAllNews(pageOffset)
                    }
                }
            }

        })
    }

    fun configureSearchView() {
        binding.etSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.isNotEmpty()) {
                    viewModel.searchNews(newText.toString())
                } else {
                    state = false
                    pageOffset = 1
                    viewModel.getAllNews(pageOffset)
                }
                return true
            }
        })
    }

    fun configureSwipe(){
        binding.swipe.setOnRefreshListener {
            if (state){
                pageOffset = 1
                viewModel.getAllNews(pageOffset)
            }
        }
    }

}


