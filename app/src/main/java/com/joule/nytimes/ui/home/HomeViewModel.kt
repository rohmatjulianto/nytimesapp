package com.joule.nytimes.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.load.engine.Resource
import com.joule.nytimes.domain.NytimesRepository
import com.joule.nytimes.local.NewsDao
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.model.Resources
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(
    val repo: NytimesRepository,
    val newsDao: NewsDao,
    private val dispatcher:CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _news = MutableLiveData<Resources<List<NewsEntity>>>()
    val news : LiveData<Resources<List<NewsEntity>>> = _news

    init {
        getAllNews(1)
    }

    fun getAllNews(offset: Int){
        viewModelScope.launch(dispatcher) {
            val result = repo.getAllNews(offset)
            _news.postValue(result)
        }
    }

    fun getAllNewsSaved(offset: Int){
        viewModelScope.launch(dispatcher) {
            val result = newsDao.getAllNews(offset *10)
            _news.postValue(Resources.Success(result))
        }
    }

    fun searchNews(title: String){
        viewModelScope.launch(dispatcher) {
            val result = newsDao.findByTitle(title)
            _news.postValue(Resources.Success(result))
        }
    }
}