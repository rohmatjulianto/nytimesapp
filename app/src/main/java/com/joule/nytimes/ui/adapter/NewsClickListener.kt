package com.joule.nytimes.ui.adapter

interface NewsClickListener {
    fun onClickNewsTimes(title: String?)
}