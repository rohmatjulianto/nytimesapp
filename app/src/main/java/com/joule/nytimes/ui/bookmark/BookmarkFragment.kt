package com.joule.nytimes.ui.bookmark

import android.content.res.Configuration
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.joule.nytimes.R
import com.joule.nytimes.databinding.FragmentBookmarkBinding
import com.joule.nytimes.ui.adapter.NewsAdapter
import com.joule.nytimes.ui.adapter.NewsClickListener
import com.joule.nytimes.ui.home.HomeFragmentDirections
import org.koin.android.ext.android.inject

class BookmarkFragment : Fragment(), NewsClickListener {

    lateinit var binding: FragmentBookmarkBinding
    val viewModel: BookmarkViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBookmarkBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureSwipe()

        val newsAdapter = NewsAdapter(this)
        binding.rvList.apply {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                layoutManager = GridLayoutManager(context, 2)
            } else {

                layoutManager = LinearLayoutManager(context)
            }
            adapter = newsAdapter
        }


        viewModel.news.observe(viewLifecycleOwner) {
            binding.swipe.isRefreshing = false
            newsAdapter.submitList(it)
        }
    }

    override fun onClickNewsTimes(title: String?) {
        val action = BookmarkFragmentDirections.actionNavigationBookmarkToDetailActivity(title)
        findNavController().navigate(action)
    }

    fun configureSwipe(){
        binding.swipe.setOnRefreshListener {
            viewModel.getAllFavorite()
        }
    }
}