package com.joule.nytimes.ui.bookmark

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.joule.nytimes.local.NewsDao
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.model.Resources
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookmarkViewModel(
    val newsDao: NewsDao,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _news = MutableLiveData<List<NewsEntity>>()
    val news : LiveData<List<NewsEntity>> = _news

    init {
        getAllFavorite()
    }
    
    fun getAllFavorite(){
        viewModelScope.launch(dispatcher) {
            val result = newsDao.getAllNewsFav()
            _news.postValue(result)
        }
    }
}