package com.joule.nytimes.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.joule.nytimes.databinding.NewsItemBinding
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.utils.ExtensionsUtils.loadImage

class NewsAdapter(val listener: NewsClickListener): ListAdapter<NewsEntity, NewsAdapter.NewsViewHolder>(NEWS_COMPARATOR) {

    class NewsViewHolder(val binding: NewsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(news: NewsEntity) {
            binding.apply {
                tvTitle.text = news.title
                tvContent.text = news.content
                imgThumbnail.loadImage(news.thumbnail)
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = NewsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = getItem(position)
        news?.let { news ->
            holder.onBind(news)
            holder.binding.root.setOnClickListener{
                listener.onClickNewsTimes(news.title)
            }
        }
    }

    companion object {
        private val NEWS_COMPARATOR = object : DiffUtil.ItemCallback<NewsEntity>() {
            override fun areItemsTheSame(oldItem: NewsEntity, newItem: NewsEntity): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: NewsEntity, newItem: NewsEntity): Boolean {
                return oldItem == newItem
            }

        }
    }
}
