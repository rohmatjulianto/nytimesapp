package com.joule.nytimes.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.lifecycle.ViewTreeLifecycleOwner
import androidx.navigation.navArgs
import com.joule.nytimes.R
import com.joule.nytimes.databinding.ActivityDetailBinding
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.utils.ExtensionsUtils.loadImage
import com.joule.nytimes.utils.ExtensionsUtils.showToast
import org.koin.android.ext.android.inject

class DetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetailBinding
    val viewModel: DetailViewModel by inject()
    val args: DetailActivityArgs by navArgs()
    var news : NewsEntity? = null
    var saveState: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        args.title?.let{
            viewModel.getDetail(it)
            viewModel.verifySaved(it)
            setTitle(args.title)
        }

        viewModel.news.observe(this){
            news = it
            binding.apply {
                tvTitle.text = it.title
                tvContent.text = it.content
                imgThumbnail.loadImage(it.thumbnail)
            }
        }

        viewModel.isSaved.observe(this){
            saveState = it
            if (it != 0){
                binding.fabSaved.setImageDrawable(getDrawable(R.drawable.ic_baseline_bookmark_24))
            }else{
                binding.fabSaved.setImageDrawable(getDrawable(R.drawable.ic_baseline_bookmark_border_24))
            }
        }

        binding.fabSaved.setOnClickListener{
            news?.let {
                if (saveState != 0){
                    it.isFav = 0
                    this.showToast("${it.title} Deleted")
                }else{
                    it.isFav = 1
                    this.showToast("${it.title} Saved")
                }
                viewModel.savedBookmarked(it)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}