package com.joule.nytimes.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.joule.nytimes.local.NewsDao
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.model.Resources
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(
    val newsDao: NewsDao,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _news = MutableLiveData<NewsEntity>()
    val news: LiveData<NewsEntity> = _news

    private val _isSaved = MutableLiveData<Int>()
    val isSaved: LiveData<Int> = _isSaved

    fun getDetail(title: String) {
        viewModelScope.launch(dispatcher) {
            val result = newsDao.getAllNewsDetail(title)
            _news.postValue(result)
        }
    }

    fun verifySaved(title: String) {
        viewModelScope.launch(dispatcher) {
            val result = newsDao.findIsSaved(title)
            _isSaved.postValue(result)
        }
    }

    fun savedBookmarked(news: NewsEntity) {
        viewModelScope.launch(dispatcher) {
            newsDao.updateFav(news)
            news.title?.let {
                verifySaved(it)
            }
        }
    }
}