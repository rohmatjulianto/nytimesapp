package com.joule.nytimes.model

sealed class Resources<T>(val data: T? = null, val e: Exception? = null, val msg: String? = null) {
    class Success<T>(data: T?): Resources<T>(data)
    class Error<T>(e: Exception): Resources<T>(e = e)
    class Fault<T>(msg: String): Resources<T>(msg = msg)
}

