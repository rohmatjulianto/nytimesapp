package com.joule.nytimes.model

import com.google.gson.annotations.SerializedName

data class NewsTimes(
    @SerializedName("title")
    val title: String?,

    @SerializedName("section")
    val section: String?,

    @SerializedName("abstract")
    val content: String?,

    @SerializedName("thumbnail_standard")
    val thumbnail: String?,

    @SerializedName("url")
    val url: String?
)
