package com.joule.nytimes.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.joule.nytimes.local.AppDatabase
import com.joule.nytimes.local.NewsDao
import org.koin.dsl.module

val dbModule = module {
    single { provideDb(get()) }
    single { provideNewsDao(get()) }
}

fun provideDb(application: Application): AppDatabase {
    val db = Room.databaseBuilder(
        application, AppDatabase::class.java, "news-db"
    ).fallbackToDestructiveMigration().build()
    return db
}

fun provideNewsDao(db: AppDatabase): NewsDao = db.newsDao()