package com.joule.nytimes.module

import com.joule.nytimes.ui.bookmark.BookmarkViewModel
import com.joule.nytimes.ui.detail.DetailViewModel
import com.joule.nytimes.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { HomeViewModel(get(), get()) }
    viewModel { DetailViewModel(get()) }
    viewModel { BookmarkViewModel(get()) }
}