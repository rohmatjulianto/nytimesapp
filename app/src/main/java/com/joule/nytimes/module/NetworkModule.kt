package com.joule.nytimes.module

import com.google.gson.Gson
import com.joule.nytimes.BuildConfig
import com.joule.nytimes.domain.NytimesApi
import com.joule.nytimes.utils.Constant.NETWORK_TIMEOUT
import com.joule.nytimes.utils.TokenAuthenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    single { TokenAuthenticator(get()) }
    single { provideOkHttpClient(get()) }
    single { Gson() }
    single { provideRetrofit(get(), BuildConfig.BASE_URL) }
    single { provideMainApi(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .baseUrl(url)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

fun provideOkHttpClient(tokenAuthenticator: TokenAuthenticator) =
    OkHttpClient().newBuilder()
        .connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(
            Interceptor { chain ->
                val request = chain.request()
                val httpUrl = request.url.newBuilder().addQueryParameter("api-key", BuildConfig.API_KEY).build()
                chain.proceed(request.newBuilder().url(httpUrl).build())
            }
        )
        .addInterceptor(HttpLoggingInterceptor().apply {
            level =
                HttpLoggingInterceptor.Level.BODY
        })
        .authenticator(tokenAuthenticator)
        .build()


private fun provideMainApi(retrofit: Retrofit): NytimesApi = retrofit.create(NytimesApi::class.java)
