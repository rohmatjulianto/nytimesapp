package com.joule.nytimes.module

import com.joule.nytimes.domain.NytimesRepository
import com.joule.nytimes.domain.NytimesRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<NytimesRepository>{NytimesRepositoryImpl(get(), get())}
}