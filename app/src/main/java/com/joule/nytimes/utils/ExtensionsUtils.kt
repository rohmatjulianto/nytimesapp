package com.joule.nytimes.utils

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide

object ExtensionsUtils {
    fun ImageView.loadImage(url:String?) {
        Glide.with(this.context)
            .load(url)
            .centerCrop()
            .into(this)
    }

     fun Context?.showToast(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}