package com.joule.nytimes.utils

import android.content.Context
import android.widget.Toast
import com.joule.nytimes.utils.ExtensionsUtils.showToast
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator(val context: Context) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        context.apply {
            showToast("invalid api-key")
        }
        return null
    }
}