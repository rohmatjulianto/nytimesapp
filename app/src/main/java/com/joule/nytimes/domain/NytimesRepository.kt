package com.joule.nytimes.domain

import android.util.Log
import com.joule.nytimes.local.NewsDao
import com.joule.nytimes.local.NewsEntity
import com.joule.nytimes.model.Resources

interface NytimesRepository {
    suspend fun getAllNews(offset: Int): Resources<List<NewsEntity>>
}

class NytimesRepositoryImpl(val service: NytimesApi, val newsDao: NewsDao) : NytimesRepository {
    override suspend fun getAllNews(offset: Int): Resources<List<NewsEntity>> {
        return try {
            val result = service.getAllNews(offset * 10)
            if (result.isSuccessful){
                result.body()?.data?.let { data ->
                    for (i in 0 until data.size) {
                        val newsTimes = data.get(i)
                        if (newsTimes.title?.let { newsDao.findCountByTitle(it) } == 0) {
                            newsDao.insertAll(
                                NewsEntity(
                                    title = newsTimes.title,
                                    content = newsTimes.content,
                                    section = newsTimes.section,
                                    thumbnail = newsTimes.thumbnail,
                                    url = newsTimes.url,
                                    isFav = 0,
                                )
                            )
                        }
                    }
                }
//                get from room db with pagination using limit
                Resources.Success(newsDao.getAllNews(offset * 10))
            }else{
                Resources.Fault("${result.code()} : ${result.message()}")
            }

        } catch (e: Exception) {
            Resources.Error(e)
        }

    }
}