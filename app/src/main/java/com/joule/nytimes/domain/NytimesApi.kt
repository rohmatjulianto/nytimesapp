package com.joule.nytimes.domain

import com.joule.nytimes.model.BaseResponse
import com.joule.nytimes.model.NewsTimes
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NytimesApi {
    @GET("/svc/news/v3/content/all/all.json?limit=10")
    suspend fun getAllNews(@Query("offset") offset: Int): Response<BaseResponse<ArrayList<NewsTimes>>>
}