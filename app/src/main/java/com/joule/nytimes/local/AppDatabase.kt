package com.joule.nytimes.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(NewsEntity::class), version = 10, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}