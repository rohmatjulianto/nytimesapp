package com.joule.nytimes.local

import androidx.room.*

@Dao
interface NewsDao {
    @Query("SELECT * FROM news LIMIT :limit")
    suspend fun getAllNews(limit: Int): List<NewsEntity>

    @Query("SELECT * FROM news WHERE is_favorite LIKE 1")
    suspend fun getAllNewsFav(): List<NewsEntity>

    @Query("SELECT * FROM news WHERE title LIKE :title LIMIT 1")
    suspend fun getAllNewsDetail(title: String): NewsEntity

    @Query("SELECT * FROM news WHERE title LIKE '%' || :title|| '%' ")
    suspend fun findByTitle(title: String): List<NewsEntity>

    @Query("SELECT count(*) FROM news WHERE title LIKE :title LIMIT 1")
    suspend fun findCountByTitle(title: String): Int

    @Query("SELECT count(*) FROM news WHERE title LIKE :title AND is_favorite LIKE 1 LIMIT 1")
    suspend fun findIsSaved(title: String): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(vararg news: NewsEntity)

    @Update
    fun updateFav(vararg news: NewsEntity)

    @Delete
    fun delete(news: NewsEntity)
}